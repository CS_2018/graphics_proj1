console.log("abc");

(() => {
  "use strict";

  //Variables
  var canvas;
  var textCanvas;
  var gl;
  var ctext;

  var maxNumTriangles = 20000;  
  var maxNumVertices  = 3 * maxNumTriangles;
  const circle = 360;
  var index = 0;
  var maxIndex = 50*circle;
  var t1, t2, t3, t4;
  
  //shader
  var program;
  var vBuffer;
  var vPosition
  var cBuffer;
  var vColor;

  var cIndex = 0;
  var cmax = 7;
  var bC = [ 0.8, 0.7, 0.5, 1.0 ];

  var colors = [
    vec4( 0.0, 0.0, 0.0, 1.0 ),  // 1 black
    vec4( 1.0, 1.0, 1.0, 1.0 ),  // 2 white
    vec4( 1.0, 0.0, 0.0, 1.0 ),  // 3 red
    vec4( 1.0, 1.0, 0.0, 1.0 ),  // 4 yellow
    vec4( 0.0, 1.0, 0.0, 1.0 ),  // 5 green
    vec4( 0.0, 0.0, 1.0, 1.0 ),  // 6 blue
    vec4( 1.0, 0.0, 1.0, 1.0 ),  // 7 magenta
    vec4( 0.0, 1.0, 1.0, 1.0 )   // 8 cyan
  ];

  function init() {
    canvas = document.getElementById( "gl-canvas" );
    textCanvas= document.getElementById('text_canvas');
    gl = WebGLUtils.setupWebGL( canvas );
    ctext = textCanvas.getContext('2d');
    if ( !gl ) { alert( "WebGL isn't available" ); }
    // Set text Attributes
    ctext.font = "18px Verdana";
    ctext.textAlign = "center";
    //  Set Background Colors
    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( bC[0], bC[1],  bC[2],  bC[3],  );
    gl.clear( gl.COLOR_BUFFER_BIT );


    //
    //  Load shaders and initialize attribute buffers
    //
    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );
      
      
    vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, 8*maxNumVertices, gl.STATIC_DRAW);
    
    vPosition = gl.getAttribLocation( program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);
    
    cBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, 16*maxNumVertices, gl.STATIC_DRAW );
    
    vColor = gl.getAttribLocation( program, "vColor");
    gl.vertexAttribPointer(vColor, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vColor);
      
    //  Add Listener to Start Button
    var m = document.getElementById("startButton");
    
    m.addEventListener("click", function() {
       //alert( "GameStarted" );
       m.disabled = true;
       m.classList.add("disabled");
       ctext.clearRect(0, 0, canvas.width, canvas.height);
		   main();

       //draw_Circle(0,0,0.8,[0.05, 0.1, 0.05, 0.5]);
       //drawGame();
    });
    //Screen Click
    function my_click_function(event) {
      //console.log(event);
      var x,y;

      var r = 0.2;
      //draw_Text("HelloWorld");
      //gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer);

      //Position relative to element
      let rect = event.target.getBoundingClientRect();
      let x_raw = event.clientX - rect.left;
      let y_raw = event.clientY - rect.top;

      x = x_raw / canvas.width * 2 - 1;
      y = y_raw / canvas.height * -2 + 1;
	  
		  mouseClick(x, y, x_raw, y_raw);

     // draw_Circle(x,y,r,colors[cIndex]);
     // clear_window();

      cIndex++;
      if(cIndex>cmax){
        cIndex=0;
      }
    };

    canvas.addEventListener("click", my_click_function);

    render();

    
  }

  window.addEventListener("load", init);

  function draw_Circle(x,y,r,color){
  //Vertex Info      
  if(index>maxIndex) {
    return;
  }
	//console.log("Draw Called",x,y,r,color);
	gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer);

      for (let i = 0; i < circle; i++) {
        let rad = i*Math.PI/180;

        var y1 = r*Math.sin(rad)+y;
        var x1 = r*Math.cos(rad)+x;

        t2 = vec2(x1, y1);

        gl.bufferSubData(gl.ARRAY_BUFFER, 8*(index+i), flatten(t2));
      }

      gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer);

      for (let i = 0; i < circle; i++) {
        //set Vertex Colors
        let t = vec4(color);
        gl.bufferSubData(gl.ARRAY_BUFFER, 16*(index+i), flatten(t));
      }
      index+=circle;
  }
  function clear_window(){
    gl.clearColor( bC[0], bC[1],  bC[2],  bC[3],  );
    gl.clear( gl.COLOR_BUFFER_BIT );
    index=0;
  }
  function draw_Text(sampleText1, sampleText2){
    ctext.clearRect(0, 0, canvas.width, canvas.height);
    ctext.font = '42px Impact';
    ctext.fillText(sampleText1, 250, 230);
    ctext.font = '24px Impact';
    ctext.fillText(sampleText2, 250, 270);
  }
  

  function render() {

    gl.clear( gl.COLOR_BUFFER_BIT );

    for(var i = 0; i<index; i+=circle) {
      gl.drawArrays( gl.TRIANGLE_FAN, i, circle );
    }

    window.requestAnimFrame(render);
  }

  
  window.draw_circle = draw_Circle;
  window.clear_window = clear_window;
  window.draw_Text = draw_Text;
  
})();
