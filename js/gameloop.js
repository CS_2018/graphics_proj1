var virusClusters = [];
var particleEffect = [];
var numClusters = 7;							//some initial amount of viruses
var numParticles = 5;
var winCondition = false;
var loseCondition = false;
var score = 100;

function main(){
	var circ = new Circle(0.8, [0, 0], vec4(1,1,1,1));
	var border = new Circle(0.825, [0, 0], vec4(0,0,0,1));
	var virusPositions = [];						//initial virus spawn positions
	score = 100;
	
	const convertRadians = 180 / Math.PI;
	
	//assigns spawn positions for the virus based on a random position in each quadrant of the circle
	for(let i = 1; i <= numClusters; i++){
		var quadrantPosition = (Math.random() * 45 + 90 * (i % 4)) / convertRadians;
		var x = Math.sin(quadrantPosition) * circ.getRadius();
		var y = Math.cos(quadrantPosition) * circ.getRadius();
		virusPositions[i - 1] = [x,y];
	}
	
	//draws our initial circle
	border.draw();
	circ.draw();
		
	//creates virus clusters and draws them onscreen
	for(let i = 0; i < numClusters; i++){
		virusClusters[i] = new Virus(0.1, virusPositions[i], vec4(Math.random() / 3, 0.4 + (Math.random() * 0.6), (0.7 * Math.random()) + 0.3, 1.0));
		virusClusters[i].draw();
	}
	
	var loop_timer;
	
	function loop() {
		clear_window();
		border.draw();
		circ.draw();

		let surfaceArea = 0;
		
		virusClusters.forEach( (element, i) => {			
			surfaceArea += element.getArea();
			
			if(element.isAlive()){
				element.grow(0.001  * (numClusters - virusClusters.length + 1));
				element.draw();
			}
		} );
		
		particleEffect.forEach( (element) => {
			element.update();
		} );
				
		
		let redness = 1 - (surfaceArea / (circ.getArea() * 1.25));
		circ.setColour(1.0, redness, redness, 1.0);

		mergeVirus();

		
		particleEffect = cull(particleEffect);
		virusClusters = cull(virusClusters);
				
		if(virusClusters.length == 0){
			score = Math.floor(score) + 10;
			document.getElementById("points").textContent = score;
			
			draw_Text("GAME WIN!", "Virus has been eradicated!");
			winCondition = true;
			
			document.getElementById("startText").textContent = "Play Again?";
			document.getElementById("startButton").disabled = false;
			document.getElementById("startButton").classList.remove("disabled");
			
			clear_window();
			border.draw();
			circ.draw();
			return;
		} else if (redness <= 0) { 
			draw_Text("ALL HOPE IS LOST!", "The virus has eradicated everyone you care about!");
			score = 0;
			loseCondition = true;
			
			document.getElementById("startText").textContent = "Play Again?";
			document.getElementById("startButton").disabled = false;
			document.getElementById("startButton").classList.remove("disabled");
			
			clear_window();
			border.draw();
			circ.draw();
			
			return;
		}
		
		score -= (1 - redness) * 3;
		
		if(score < 0){
			score = 0;
		}
		
		document.getElementById("points").textContent = Math.floor(score);
		loop_timer = setTimeout(loop, 100);
	}
	
	loop();
	
	if(winCondition || loseCondition){
		console.log("Play again?");
	}
	return;
}

class Circle{	
	constructor(r, p, c){
		this.radius = r;
		this.position = {x:p[0], y:p[1]};
		this.colour = c;
				
		this.getRadius = function() { return this.radius; };
		this.getPos = function() { return this.position; };
		this.getArea = function() { return ((this.radius * this.radius) * Math.PI) / 2; };
		this.setColour = function(r, g, b, a) { this.colour = vec4(r, g, b, a); };
		this.setRadius = function(value) { this.radius = value; };
		this.setPos = function(position) { this.position = {x: position[0], y: position[1]}; };
		
		this.draw = function() {			
			draw_circle(
				this.position.x,
				this.position.y,
				this.radius,
				this.colour
				); };
	}
}

class Virus extends Circle{
	constructor(r, p, c){ 
		super(r, p, c);
		
		this.alive = true;

		this.grow = function(amount){ this.radius += amount; };
		this.shrink = function(amount){ this.radius -= amount; };
		this.isAlive = function() { return this.alive; };
		this.kill = function() { this.alive = false; };
	}
}

class Particle extends Circle{
	constructor(r, p, c, v){
		super(r, p, c);
		
		this.vector = v;
		this.alive = true;
		this.ticks = 0;
		
		this.update = function(){
			this.position.x += this.vector.x;
			this.position.y += this.vector.y;
			this.ticks += 1;
			
			this.colour = vec4(1 - (this.ticks / 5),0,0, 1 - (this.ticks / 5));
			
			if(this.ticks < (3 + (Math.random() * 3))){
				this.draw();
			} else this.alive = false;
		};
		
		this.isAlive = function(){ return this.alive; };
	}
}

function cull(enemyStack){
	let temp = [];
	while(enemyStack.length != 0){
		let currentEnemy = enemyStack.pop();
		if(currentEnemy.isAlive()){
			temp.push(currentEnemy);
		}
	}
	
	return temp;
}

function generateParticles(x, y){
	let particles = [];
	for(let i = 0; i < numParticles; i++){
		particles.push(new Particle(Math.random() * 0.03332, [x, y], vec4(1,0,0,1), {x: (Math.random() / 5) - 0.1, y: (Math.random() / 5) - 0.1}));
	}
	
	return particles;
}
//Generate particles and check if hitting viruse cluster
function mouseClick(x, y, x_raw, y_raw) {
	particleEffect = generateParticles(x, y);
	
	virusClusters.forEach( (element, i) => {
			if(Math.hypot(x - element.getPos().x, y - element.getPos().y) < element.getRadius()){
				element.shrink(0.0275);
				
				if(element.getRadius() <= 0.01){
					element.kill();
				}
			}
		} );
}
//Detect virus colisions 
function mergeVirus(){
	virusClusters.forEach( (element, index) => {
		for(let i = index + 1; i < virusClusters.length; i++){
			let temp = Math.hypot(
				virusClusters[i].getPos().x - element.getPos().x,
				virusClusters[i].getPos().y - element.getPos().y
			);
			let temp2 = element.getRadius()+virusClusters[i].getRadius();

			console.log(temp, temp2);

			if(temp < temp2){
				console.log(index);

				let x_avg = (element.getPos().x + virusClusters[i].getPos().x) / 2;
				let y_avg = (element.getPos().y + virusClusters[i].getPos().y) / 2;

				let angle = Math.atan2(y_avg, x_avg);




				// let angle = (
				// 	Math.atan2(
				// 		element.getPos().y,
				// 		element.getPos().x
				// 	) + Math.atan(virusClusters[i].getPos().y/virusClusters[i].getPos().x))/2

				let myX = Math.cos(angle) * 0.8;
				let myY = Math.sin(angle) * 0.8;
				let myR = (element.getRadius() + virusClusters[i].getRadius())/1.25;
				element.kill();

				virusClusters[i].setRadius(temp2 / 1.25);
				virusClusters[i].setPos([myX, myY]);			
			}
		}
	} );
}